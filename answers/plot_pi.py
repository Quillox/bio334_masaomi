import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

ahal = pd.read_table("pi_td_ahal.dat")
alyr = pd.read_table("pi_td_alyr.dat")
ahal = ahal.set_index('GeneID')
alyr = alyr.set_index('GeneID')
plt.plot(ahal['pi'])
plt.plot(alyr['pi'])
plt.xticks(rotation=45)
plt.ylabel("pi")
plt.show()

