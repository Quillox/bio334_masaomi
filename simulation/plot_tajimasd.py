import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df = pd.read_table("result.dat")
data = df['Tajimas_D']
plt.plot(data)
plt.xlabel('Generation')
plt.ylabel('Tajimas_D')
plt.show()

