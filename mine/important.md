# Tajimas D > 0


$\pi > s/\sum_{i=1}^{n-1}{\frac{1}{i}}$

* Many higher heterozygous sites
  * Population size decreases rapidly (bottleneck effect, founder effect)
      * singleton may be removed from the population
  * Balancing selection (heterozygote advantage)


# Tajimas D < 0

  $\pi < s/\sum_{i=1}^{n-1}{\frac{1}{i}}$

* Many lower heterozygous sites
  * Population size increases rapidly
      * Singleton may be introduced in the population
  * Positive/Negative selection (purifying selection, selective sweep, directional selection)

